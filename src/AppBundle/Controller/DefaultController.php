<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Badge;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/administration/create-badge", name="create-badge")
     */
    public function createBadgeForm(Request $request)
    {
        $badge = new Badge();

        $form = $this->createFormBuilder($badge)
            ->setAction('/administration/create-badge-action' . $request->get('badge_id'))
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            //->add('image', TextType::class)
            ->add(
                'icon',
                ChoiceType::class,
                array(
                    'choices'      => $this->getIconChoices(),
                   /* 'choice_label' => function ($value, $key, $index) {
                        return str_replace('&amp;', '&', $key); //"&amp;", "&"
                    },*/
                )
            )
            ->add('pointValue', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Badge!', 'attr' => array('class' => 'mb-xl mt-xl mr-xs btn btn-lg btn-primary')))
            ->getForm();

        return $this->render(
            'administration/new-badge.html.twig',
            array(
                'form'      => $form->createView(),
                'page_name' => 'Create Badge',
            )
        );
    }

    private function getIconChoices()
    {
        $icons = array();
        $icons['&#xf036; Glass'] = 'fa-glass';
        $icons['&#xf036; Music'] = 'fa-music';
        $icons['&#xf063; Search'] = 'fa-search';
        ksort($icons);
        return $icons;
    }
}
